# darwin-nix-config

My flake-based nix configuration on darwin/MacOS.  This is working, but should be considered an ongoing work in progress.

## Installing Nix 

1. install the package manager: note that this should be a version of the package manager with support for flakes.
- curl -L https://nixos.org/nix/install | sh

2. install nix-darwin: note that nix-darwin does not presently support flakes out of the box, so I've elected to
bootstrap nix-darwin using the installer:
- nix-build https://github.com/LnL7/nix-darwin/archive/master.tar.gz -A installer
- ./result/bin/darwin-installer

3. update default darwin configuration (~/.nixpkgs/darwin-configuration.nix)
- add required configuration options for flake support, add users to the generated darwin-configuration.nix
```
{
  ...
  nix = {
   extraOptions = ''
       experimental-features = nix-command flakes
       keep-outputs = true
       keep-derivations = true       
     '';
  };

  users.users.anakos = {
    name = "Alex Nakos";
    home = "/Users/anakos";
  };
  ...
}
```

4. update nix channels
- nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
- nix-channel --add https://nixos.org/channels/nixpkgs-unstable

5. build nix-darwin
- ./result/sw/bin/darwin-rebuild build
- ./result/sw/bin/darwin-rebuild switch

6. create + initialize flaked configuration (or just clone this project at ~/.config/darwin)
- mkdir -p ~/.config/darwin
- cd ~/.config/darwin
- nix flake init
- modify the flake configuration

7. build and switch to the flake configuration
- nix build ~/.config/darwin\#darwinConfigurations.nj-dev-m-19243.system
- ./result/sw/bin/darwin-rebuild switch --flake ~/.config/darwin

8. switch to fish shell (optional, only if using fish)
- update /etc/shells to to include fish by adding /etc/profiles/per-user/anakos/bin/fish
- chsh -s /etc/profiles/per-user/$USER/bin/fish

## Uninstalling Nix

see https://gist.github.com/chriselsner/3ebe962a4c4bd1f14d39897fc5619732 as reference.

0. run the nix-darwin uninstaller:
- nix-build https://github.com/LnL7/nix-darwin/archive/master.tar.gz -A uninstaller
- ./result/bin/darwin-uninstaller

1. export PATH=/usr/sbin:$PATH

2. sudo launchctl unload /Library/LaunchDaemons/org.nixos.nix-daemon.plist

3. sudo vifs
- delete the line LABEL=Nix\040Store /nix apfs rw,nobrowse

4. diskutil apfs deleteVolume Nix\ Store

5. sudo nano /etc/synthetic.conf
- delete the line "nix"

6. restart and verify /nix is gone

7. purge daemon, users and groups
- USERS=$(sudo dscl . list /Users | grep nixbld)

- for USER in $USERS; do
    sudo /usr/bin/dscl . -delete "/Users/$USER"
    sudo /usr/bin/dscl . -delete /Groups/staff GroupMembership $USER;
done

- sudo /usr/bin/dscl . -delete "/Groups/nixbld"

7. rm -rf ~/.nix-*

8. sudo mv /etc/bashrc.backup-before-nix /etc/bashrc

9. sudo mv /etc/zshrc.backup-before-nix /etc/zshrc

10. sudo rm -rf /etc/nix /nix /var/root/.nix-profile /var/root/.nix-defexpr /var/root/.nix-channels ~/.nix-profile ~/.nix-defexpr ~/.nix-channels ~/.cache/nix ~/.config/nixpkgs ~/.nixpkgs ~/.config/darwin
