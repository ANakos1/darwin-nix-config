{ pkgs, lib, ... }: {
  nix = {
   package = pkgs.nixVersions.latest;# pkgs.nixUnstable;
   extraOptions =
     ''
       experimental-features = nix-command flakes
       keep-outputs = true
       keep-derivations = true       
       extra-platforms = x86_64-darwin aarch64-darwin
     '';
  };

  users.users.anakos = {
    name = "anakos";
    home = "/Users/anakos";
  };

  services.nix-daemon.enable = true;

  # TODO: no longer necessary?
  system.stateVersion = 4;
}