{ pkgs, lib, ... }: {
  nixpkgs = {
    # overlays = [];

    config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
      "vscode"
    ];
  };

  home = {
    # NOTE: stateVersion is required on this system in order to maintain purity
    # and prevent loading of system <nixpkgs>
    stateVersion = "22.05";
    packages = [
      pkgs.curl
      pkgs.gnupg
      pkgs.direnv
      pkgs.emacs # TODO: import emacs config from precision laptop
      pkgs.fish
      pkgs.git
      pkgs.gnugrep
      pkgs.htop
      # pkgs.nixUnstable # to make flakes work
      pkgs.nixVersions.latest
      pkgs.openssh
      pkgs.ripgrep
      pkgs.tree
      pkgs.wget
      pkgs.vscode
    ];
  };
  programs = {
    home-manager.enable = true;
    direnv = {
      enable                = true;
      nix-direnv.enable     = true;
      # enableFishIntegration = true;
      enableBashIntegration = true;
      enableZshIntegration  = true;
    };
    fish = {
      enable         = true;
      loginShellInit = ''
        if test -e /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh
          fenv source /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh
        end
        if test -e /nix/var/nix/profiles/default/etc/profile.d/nix.sh
          fenv source /nix/var/nix/profiles/default/etc/profile.d/nix.sh
        end
        fish_add_path /etc/profiles/per-user/$USER/bin
	      fish_add_path /run/current-system/sw/bin
        eval (direnv hook fish | source)
      '';
      plugins = [
        {
          name = "foreign-env";
          src = pkgs.fetchFromGitHub {
            owner  = "oh-my-fish";
            repo   = "plugin-foreign-env";
            rev    = "dddd9213272a0ab848d474d0cbde12ad034e65bc";
            sha256 = "00xqlyl3lffc5l0viin1nyp819wf81fncqyz87jx8ljjdhilmgbs";
          };
        }
      ];
    };
    git = {
      enable = true;
      userName  = "Alexander Nakos";
      extraConfig = {
        core.editor = "emacs";
      };
    };
    gpg = {
      enable = true;
    };
    ssh = {
      enable      = true;
      extraConfig = ''
Host gitlab.com
     Hostname gitlab.com
     User git
     AddKeysToAgent yes
     IdentityFile ~/.ssh/williamhill/id_ed25519

Host github.com
     Hostname github.com
     User git
     AddKeysToAgent yes
     IdentityFile ~/.ssh/github/id_ed25519
      '';
    };
    vscode = {
      enable     = true;
      extensions = pkgs.vscode-utils.extensionsFromVscodeMarketplace [
        {
          name = "dhall-lang";
          publisher = "dhall";
          version = "0.0.4";
	  sha256 = "7vYQ3To2hIismo9IQWRWwKsu4lXZUh0Or89WDLMmQGk=";
        }
        {
          name = "vscode-dhall-lsp-server";
          publisher = "dhall";
          version = "0.0.4";
	  sha256 = "WopWzMCtiiLrx3pHNiDMZYFdjS359vu3T+6uI5A+Nv4=";
        }
        {
          name = "nix-env-selector";
          publisher = "arrterian";
          version = "1.0.9";
          sha256 = "TkxqWZ8X+PAonzeXQ+sI9WI+XlqUHll7YyM7N9uErk0=";
        }
/*	
        {
          name = "githistory";
          publisher = "donjayamanne";
          version = "0.6.19";
          sha256 = "15s2mva9hg2pw499g890v3jycncdps2dmmrmrkj3rns8fkhjn8b3";
        }
*/
        {
	  name = "gitlens";
          publisher = "eamodio";
          version = "2024.4.1305";
          sha256 = "ivKxkfDfDBR1TBVlZtCxMf2rT1+Tox3r69eyj/ZUcg0=";	  	        }
        {
          name = "code-runner";
          publisher = "formulahendry";
          version = "0.11.6";
          sha256 = "1q1gdzjgvksmnwrd7ikg87b4wvddwkxdk577jrfwijcqqh7zspc8";
        }
        {
          name = "haskell";
          publisher = "haskell";
          version = "2.5.0";
	  sha256 = "WolhQ+D1rIcvoY1+1coEo0pnORv0+RA8Vti1P+vg2LY=";
        }
        {
          name = "language-haskell";
          publisher = "justusadam";
          version = "3.6.0";
          sha256 = "rZXRzPmu7IYmyRWANtpJp3wp0r/RwB7eGHEJa7hBvoQ=";
        }
        {
          name = "vscode-docker";
          publisher = "ms-azuretools";
          version = "1.26.0";
	  sha256 = "RkHKO97so3N61UXc35IekmLEsdPQIOpSxiDzCGL3VTs=";
        }
        {
          name = "scala";
          publisher = "scala-lang";
          version = "0.5.7";
	  sha256 = "cjMrUgp2+zyqT7iTdtMeii81X0HSly//+gGPOh/Mfn4=";
        }
        {
          name = "metals";
          publisher = "scalameta";
	  version = "1.39.6";
	  sha256 = "yVadk3S+/KCFpffHmf94qxLPsK9fCs8d4+sQj2JEoEQ=";
        }
        {
          name = "rust-analyzer";
          publisher = "rust-lang";
	  version = "0.4.2025";
	  sha256 = "9tNjT+leH+A/8ZINnLhDwb3hVFd21V7Vai6p0Cskfl0=";
        }
      ];
    };
  };
}
