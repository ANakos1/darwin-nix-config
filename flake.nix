{
  # pilfered in part from https://github.com/LnL7/nix-darwin/blob/e1a3f7292f085fd588d11f94ed0f47968c16df0c/modules/examples/flake.nix
  description = "nix-darwin configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    darwin = {
      url = "github:lnl7/nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, darwin, home-manager, nixpkgs, ...  }@inputs:
    {
      darwinConfigurations."M1X6157R9C" =
        darwin.lib.darwinSystem {
          # system = "x86_64-darwin";
	  system = "aarch64-darwin";
	  inputs = { inherit home-manager nixpkgs darwin; };
          modules = [
	    ./configuration.nix
            home-manager.darwinModule
	    {      
              # home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;	    
	      home-manager.users.anakos = ./home.nix;
            }
	  ];
        };
      # Expose the package set, including overlays, for convenience.
      darwinPackages = self.darwinConfigurations."M1X6157R9C".pkgs;
  };
}
